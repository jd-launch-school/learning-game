/*
  This file is part of Learning Game.

  Learning Game is free software: you can redistribute
  it and/or modify it under the terms of the GNU Affero General Public License
  as published by the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Learning Game is distributed in the hope that it will
  be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with Learning Game.  If not,
  see <https://www.gnu.org/licenses/>.
*/

import React, { Component } from "react";
import Question from "./question";
import Answer from "./answer";
import Button from "./button";

export default class Game extends Component {
  constructor(props) {
    super(props);

    this.state = {
      answerButtonClicked: false,
      randomQuestion: this.randomQuestion(this.props.questions)
    };
  }

  randomQuestion = questions => {
    return questions[Math.floor(Math.random() * questions.length)];
  };

  handleClick = e => {
    e.preventDefault();

    this.setState({
      answerButtonClicked: true
    });
  };

  handleNext = e => {
    e.preventDefault();

    this.setState({
      answerButtonClicked: false,
      randomQuestion: this.randomQuestion(this.props.questions)
    });
  };

  render() {
    return (
      <div>
        {this.props.started && !this.state.answerButtonClicked && (
          <Button
            onClick={this.handleClick}
            buttonText="Show Answer"
            buttonID="answer-button"
            buttonClass="answer-button"
          />
        )}

        {this.props.started && this.state.answerButtonClicked && (
          <Button
            onClick={this.handleNext}
            buttonText="Next Question"
            buttonID="next-button"
            buttonClass="next-button"
          />
        )}

        {this.props.started && (
          <Question question={this.state.randomQuestion.question} />
        )}

        {this.props.started && (
          <Answer
            answer={this.state.randomQuestion.answer}
            answerButtonClicked={this.state.answerButtonClicked}
          />
        )}
      </div>
    );
  }
}
