/*
  This file is part of Learning Game.

  Learning Game is free software: you can redistribute
  it and/or modify it under the terms of the GNU Affero General Public License
  as published by the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Learning Game is distributed in the hope that it will
  be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with Learning Game.  If not,
  see <https://www.gnu.org/licenses/>.
*/

import React, { Component } from "react";
import { ThemeProvider, createGlobalStyle } from "styled-components";
import Button from "./button";
import Game from "./game";
import Header from "./header";
import GameInfo from "./game-info";
import QuestionInfo from "./question-info";
import questions from "./data/question-list.json";

const theme = {
  primaryBackgroundColor: "rgb(255, 254, 255)",
  primaryTextColor: "rgba(0, 0, 0, 0.75)",
  secondaryTextColor: "rgba(255, 254, 255, 1)",
  primaryLinkColor: "rgba(173, 26, 0, 1)",
  primaryTextSize: "7vmin",
  border: "5px solid rgba(173, 26, 0, 1)",
  mediaTextSize1: "6vmin",
  mediaTextSize2: "4vmin",
  mediaTextSize3: "5vmin",
  minWidthScreenSize1: "650px",
  minWidthScreenSize2: "881px",
  minWidthScreenSize3: "2500px",
  maxWidth: "1200px",
  textMaxWidth: "700px"
};

const GlobalStyle = createGlobalStyle`
  body {
    font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", "Oxygen",
    "Ubuntu", "Cantarell", "Fira Sans", "Droid Sans", "Helvetica Neue",
    sans-serif;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    width: 100%;
    max-width: ${theme.textMaxWidth};
    margin: auto;
    color: ${theme.primaryTextColor};
    text-align: center;
    line-height: 1.75;
  }

  .root {
    height: 100vh;
    background-color: ${theme.primaryBackgroundColor};
  }

  p {
    margin: 10px auto;
  }

  a {
    color: ${theme.primaryLinkColor};
  }
`;

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      gameStarted: false
    };
  }

  handleClick = e => {
    e.preventDefault();

    this.setState({
      gameStarted: !this.state.gameStarted
    });
  };

  render() {
    return (
      <ThemeProvider theme={theme}>
        <div id="learning-game">
          <GlobalStyle />

          <Header />

          {!this.state.gameStarted && (
            <Button
              onClick={this.handleClick}
              buttonText="Start Game"
              buttonID="start-game"
              buttonClass="start-game"
            />
          )}

          {!this.state.gameStarted && <GameInfo />}

          {!this.state.gameStarted && (
            <Button
              onClick={this.handleClick}
              buttonText="Start Game"
              buttonID="start-game"
              buttonClass="start-game"
            />
          )}

          <Game started={this.state.gameStarted} questions={questions} />

          {this.state.gameStarted && (
            <>
              <Button
                onClick={this.handleClick}
                buttonText="Show Game Info"
                buttonID="restart-button"
                buttonClass="restart-button"
              />
              <QuestionInfo questions={questions} />
            </>
          )}
        </div>
      </ThemeProvider>
    );
  }
}

export default App;
