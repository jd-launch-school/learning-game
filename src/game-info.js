/*
  This file is part of Learning Game.

  Learning Game is free software: you can redistribute
  it and/or modify it under the terms of the GNU Affero General Public License
  as published by the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Learning Game is distributed in the hope that it will
  be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with Learning Game.  If not,
  see <https://www.gnu.org/licenses/>.
*/

import React from "react";
import styled from "styled-components";

const About = styled.div`
  text-align: left;
  max-width: ${props => props.theme.textMaxWidth};
  margin: 10px auto;
  padding: 0 1rem;
`;

export default () => (
  <About>
    <p>
      This is a practice / learning aide I made for the Coursera course,{" "}
      <a href="https://www.coursera.org/learn/learning-how-to-learn">
        Learning How to Learn
      </a>
      . I made the app in about 3 hours, so it's not perfect, doesn't have a lot
      of features, and doesn't have a great design.
    </p>
    <p>
      In the course, we learned about lots of interesting and helpful concepts
      and ideas about learning and memory. Some of the ideas we explored are:
    </p>
    <ul>
      <li>Chunking</li>
      <li>Procrastination</li>
      <li>Focused and Diffuse Modes of thinking</li>
      <li>Spaced Repetition</li>
      <li>Illusions of Competence</li>
      <li>Grouping and Memory Palaces</li>
    </ul>

    <p>
      This app is meant to help with memory. Specifically, by using the app, one
      will get help with chunking, deliberate practice, and transfer.
    </p>

    <h2>Two Modes of Thinking</h2>

    <p>
      It's important to go back and forth between focused and diffuse mode
      thinking when learning something new (<a href="#oakley">Oakley, p. 11</a>
      ).
    </p>

    <h3>Focused Mode</h3>

    <p>
      Focused mode thinking is direct, focused thought about an idea or concept.
      This involves paying close attention to details, following instructions,
      and sequential learning. It's rational, analytical, and sequential (
      <a href="#oakley">Oakley, p. 13</a>).
    </p>

    <h3>Diffuse Mode</h3>
    <p>
      Diffuse mode thinking is more intuitive, general, and free form. It
      involves relaxing one's attention and letting one's mind wander (
      <a href="#oakley">Oakley, p. 18</a>).
    </p>

    <h2>What is Chunking</h2>
    <p>
      Chunking is one of the most important parts of learning and memory. It's
      through chunking that we can learn new concepts and connect them to other
      concepts.
    </p>
    <p>
      Chunks are information that are bound together through meaning or use.
      They're a network of neurons that are used to firing together so you can
      think a thought or perform an action smoothly and effectively (
      <a href="#oakley">Oakley, p. 55</a>).
    </p>
    <p>
      Chunks are best built with focused, undivided attention, understanding the
      basic idea, and practice to help deepen patterns and help gain big picture
      context (<a href="#oakley">Oakley, p. 57-59</a>).
    </p>
    <p>
      Chunking is the mind putting together information through the experience
      of that information being connected. It's both the mind having the
      information and knowing where that information combines with other
      information.
    </p>

    <h2>All About Procrastination</h2>
    <p>
      Procrastination happens when one is confronted with a task that creates
      negative or uncomfortable feelings (<a href="#oakley">Oakley, p. 85</a>).
      Instead of confronting the task, the mind finds something that is more
      pleasant and focuses on that instead. When one procrastinates, one may
      feel better, but it's only temporary. This is how procrastination can
      share similarities with addiction.
    </p>

    <p>
      When working to prevent procrastination, it's important to avoid focusing
      on the product and instead focus on the process (
      <a href="#oakley">Oakley, p. 101</a>).
    </p>

    <p>
      For instance, when learning how to ride a bike, instead of coming into the
      learning with the idea that one will learn to ride a bike after 10 tries
      in one day, come into the situation with the idea that one will try five
      times each day and learn as much as one can learn during this practice.
      With this process of spaced repetition, one will eventually learn how to
      ride a bike.
    </p>

    <h2>Spaced Repetition</h2>
    <p>
      Trying to learn a new complicated concept all at one time, like cramming
      for a test, may lead to positive short-term (working) memory results, but
      does not lead to permanent or long-term memory.{" "}
      <a href="https://en.wikipedia.org/wiki/Spaced_repetition">
        Spaced repetition
      </a>{" "}
      is a technique for repeating information so that one can more efficiently
      move memories from working to long-term memory.
    </p>

    <h2>Pomodoro Technique</h2>
    <p>
      With the{" "}
      <a href="https://en.wikipedia.org/wiki/Pomodoro_Technique">
        Pomodoro Technique{" "}
      </a>
      , one can tackle procrastination head on. This technique involves focusing
      intently for 25 minutes with a 5 minute break inbetween sessions. During
      the break, it's important to give oneself a reward. Instead of worrying
      about completing the task, the Pomodoro Technique helps one focus on the
      process of 25-minute work periods followed by 5-minute breaks.
    </p>

    <h2>Works Cited</h2>

    <ul>
      <li id="oakley">
        <a href="https://barbaraoakley.com/books/a-mind-for-numbers/">
          <strong>Oakley Ph.D., Barbara</strong> - A Mind for Numbers - Penguin
          2014
        </a>
      </li>

      <li>
        <a href="https://en.wikipedia.org/wiki/Spaced_repetition">
          Spaced Repetition
        </a>
      </li>

      <li>
        <a href="https://en.wikipedia.org/wiki/Pomodoro_Technique">
          Pomodoro Technique
        </a>
      </li>
    </ul>
  </About>
);
