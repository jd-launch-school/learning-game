/*
  This file is part of Learning Game.

  Learning Game is free software: you can redistribute
  it and/or modify it under the terms of the GNU Affero General Public License
  as published by the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Learning Game is distributed in the hope that it will
  be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with Learning Game.  If not,
  see <https://www.gnu.org/licenses/>.
*/

import React from "react";
import styled from "styled-components";

const AnswerBox = styled.div`
  max-width: 400px;
  margin: auto;
`;

const Answer = styled.div`
  box-shadow: 1px 1px 5px rgba(0, 0, 0, 0.3);
  border-radius: 5px;
  max-width: 400px;
  margin: auto;
  padding: 1rem;
`;

export default props => (
  <AnswerBox>
    {props.answerButtonClicked && <Answer>{props.answer}</Answer>}
  </AnswerBox>
);
