/*
  This file is part of Learning Game.

  Learning Game is free software: you can redistribute
  it and/or modify it under the terms of the GNU Affero General Public License
  as published by the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Learning Game is distributed in the hope that it will
  be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with Learning Game.  If not,
  see <https://www.gnu.org/licenses/>.
*/

import React from "react";
import styled from "styled-components";

const Button = styled.button`
  border: 0;
  padding: 10px;
  border-radius: 5px;
  margin: 1rem;
  background-color: ${props => props.theme.primaryLinkColor};
  color: ${props => props.theme.secondaryTextColor};
  font-size: 1.3rem;

  :hover {
    cursor: pointer;
    background-color: #ccc;
  }
`;

export default ({ onClick, buttonID, buttonClass, buttonText }) => (
  <Button id={buttonID} className={buttonClass} onClick={onClick}>
    {buttonText}
  </Button>
);
