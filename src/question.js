/*
  This file is part of Learning Game.

  Learning Game is free software: you can redistribute
  it and/or modify it under the terms of the GNU Affero General Public License
  as published by the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Learning Game is distributed in the hope that it will
  be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with Learning Game.  If not,
  see <https://www.gnu.org/licenses/>.
*/

import React from "react";
import styled from "styled-components";

const Question = styled.div`
  font-size: 1.5rem;
  margin: 0 1rem 1rem 1rem;
`;

export default props => <Question>{props.question}</Question>;
