# Welcome to Learning Game 👋
[![License: AGPLv3](https://img.shields.io/badge/License-AGPLv3-yellow.svg)](https://www.gnu.org/licenses/)

> This is a practice / learning aide I made for the Coursera course, [Learning How to Learn](https://www.coursera.org/learn/).

### 🏠 [Demo](https://jd-launch-school.gitlab.io/learning-game/)

## Install

```sh
npm install
```

## Usage

```sh
npm run start
```

## Author

👤 **Jason Denzin**

* GitLab: [@jd-launch-school](https://gitlab.com/jd-launch-school)

## Show your support

Give a ⭐️ if this project helped you!


## 📝 License

Copyright © 2019 [Jason Denzin](https://github.com/jd-launch-school).

This project is [AGPLv3](https://www.gnu.org/licenses/) licensed.

***
_This README was generated with ❤️ by [readme-md-generator](https://github.com/kefranabg/readme-md-generator)_
